import React from 'react';
import Using from './FaqDropdown.jsx/Using';
import Recruiters from './FaqDropdown.jsx/Recruiters';

const Faq = () => {
  return (
    
    <div className='faq'>
    <h5>F.A.Q</h5>
    <h1>Frequently Asked Questions</h1>

    <div className='Faq-container'>
    <div className='Faq-left'>

<div className='Faq-button'>
<Using/>
</div>

<div className='Faq-button'>
<Recruiters/>
</div>

<div className='Faq-button'>
<Recruiters/>
</div> 
</div>

{/* ///////////////////////////////////////////////////////////////// */}

<div className='Faq-right'>
<div className='Faq-button'>
<Recruiters/>
</div>

<div className='Faq-button'>
<Recruiters/>
</div>

<div className='Faq-button'>
<Recruiters/>
</div>
</div>

  

</div>
</div>
  );
}

export default Faq;





