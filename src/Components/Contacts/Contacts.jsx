import React from 'react';
import './Contacts.css'

const Contacts = () => {
return (
<div>
<div className='Contacts'>
<div className='Contacts-flex'>
<div className='Contacts-left'>
<div className='box'>
<img src='' alt=''/>
<h2>Address</h2>
<p>New Bromswick</p>
<p>Canada</p>
</div>

<div className='box'>
<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Orange_Phone_Font-Awesome.svg/1024px-Orange_Phone_Font-Awesome.svg.png' alt=''/>
<h2>Call us</h2>
<p>+1 506-252-8551</p>
<p>+1 581-799-7033</p>
<p>+33 7 69 75 61 46</p>
</div>

<div className='box'>
<img src='' alt=''/>
<h2>Email us</h2>
<p>support@intevoo.app</p>
</div>

<div className='box'>
<img src='' alt=''/>
<h2>Business hours</h2>
<p>24h/24 7j/7</p>
</div>

      </div>

      <div className='Contacts-right'>
      <form>
<input type='text' name='name' placeholder='Your name'/>
<input type='text' name='email' placeholder='Your Email'/> 
<br/>
<br/>
<input type='subject' name='subject' placeholder='Your Subject'/>
<br/>
<br/>
<textarea className='texta' name='texta' cols='55' rows='5' placeholder='Your message'/> <br/>
<button>Send message</button>
</form>
</div>

</div>
</div>


    </div>
  );
}

export default Contacts;
